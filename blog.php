<!-- Including Header -->
<?php include('includes/header.php'); ?>


<h4>Welcome to website</h4>



<!-- Page Banner -->
<div id="pagebanner" class="pagebanner events">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-6">
                <h3>OUR BLOG</h3>
            </div>
            <div class="col-md-6 col-sm-6">
                <div class="breadcrumb">
                    <ul>
                        <li><a href="#">Home</a></li> 
                        <li class="active">Our Blog</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div><!-- /Page Banner -->

<!-- Page Banner -->
<div id="pagebanner" class="pagebanner events">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-6">
                <h3>ORCHID BLOG</h3>
            </div>
            <div class="col-md-6 col-sm-6">
                <div class="breadcrumb">
                    <ul>
                        <li><a href="#">Home</a></li> 
                        <li class="active">Our Blog</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div><!-- /Page Banner -->




<!-- Page Content -->
<section id="content" class="pagecontent blog">
	<div class="container">
		<div class="row">
			<div class="col-md-9 col-sm-9">
				<div class="blogListWrapper">
                    <div class="blogblock">
                        <div class="row">
                            <div class="col-md-4">
                                <figure class="thumbnail-img">
                                    <img src="images/blogimg1.jpg" alt="">
                                </figure>
                            </div>
                            <div class="col-md-8">
                                <div class="blog-details">
                                    <h4>Evangelical Orthodox Catholic Church</h4>
                                    <ul class="post-meta">
                                        <li><i class="fa fa-user"></i>Saugat Shrestha</li>
                                        <li><i class="fa fa-calendar"></i>14th March, 2019</li>
                                    </ul>
                                    <p>
                                        Contrary to popular belief, Lorem Ipsum is not simply random text. 
                                        It has roots in a piece of classical Latin literature from 45 BC, 
                                        making it over 2000 years old. Richard McClintock, a Latin professor 
                                        at Hampden-Sydney College in Virginia.
                                    </p>
                                    <a href="#" class="btn site-btn">READMORE</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="blogblock">
                        <div class="row">
                            <div class="col-md-4">
                                <figure class="thumbnail-img">
                                    <img src="images/blogimg2.jpg" alt="">
                                </figure>
                            </div>
                            <div class="col-md-8">
                                <div class="blog-details">
                                    <h4>Evangelical Free Church of America - Wikiwand</h4>
                                    <ul class="post-meta">
                                        <li><i class="fa fa-user"></i>Saugat Shrestha</li>
                                        <li><i class="fa fa-calendar"></i>14th March, 2019</li>
                                    </ul>
                                    <p>
                                        Contrary to popular belief, Lorem Ipsum is not simply random text. 
                                        It has roots in a piece of classical Latin literature from 45 BC, 
                                        making it over 2000 years old. Richard McClintock, a Latin professor 
                                        at Hampden-Sydney College in Virginia.
                                    </p>
                                    <a href="#" class="btn site-btn">READMORE</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="blogblock">
                        <div class="row">
                            <div class="col-md-4">
                                <figure class="thumbnail-img">
                                    <img src="images/blogimg3.jpg" alt="">
                                </figure>
                            </div>
                            <div class="col-md-8">
                                <div class="blog-details">
                                    <h4>Evangelical Free Church of America - Wikiwand</h4>
                                    <ul class="post-meta">
                                        <li><i class="fa fa-user"></i>Saugat Shrestha</li>
                                        <li><i class="fa fa-calendar"></i>15th Jan, 2019</li>
                                    </ul>
                                    <p>
                                        Contrary to popular belief, Lorem Ipsum is not simply random text. 
                                        It has roots in a piece of classical Latin literature from 45 BC, 
                                        making it over 2000 years old. Richard McClintock, a Latin professor 
                                        at Hampden-Sydney College in Virginia.
                                    </p>
                                    <a href="#" class="btn site-btn">READMORE</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="blogblock">
                        <div class="row">
                            <div class="col-md-4">
                                <figure class="thumbnail-img">
                                    <img src="images/blogimg4.jpg" alt="">
                                </figure>
                            </div>
                            <div class="col-md-8">
                                <div class="blog-details">
                                    <h4>Evangelical Free Church of America - Wikiwand</h4>
                                    <ul class="post-meta">
                                        <li><i class="fa fa-user"></i>Saugat Shrestha</li>
                                        <li><i class="fa fa-calendar"></i>15th Jan, 2019</li>
                                    </ul>
                                    <p>
                                        Contrary to popular belief, Lorem Ipsum is not simply random text. 
                                        It has roots in a piece of classical Latin literature from 45 BC, 
                                        making it over 2000 years old. Richard McClintock, a Latin professor 
                                        at Hampden-Sydney College in Virginia.
                                    </p>
                                    <a href="#" class="btn site-btn">READMORE</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="blogblock">
                        <div class="row">
                            <div class="col-md-4">
                                <figure class="thumbnail-img">
                                    <img src="images/blogimg2.jpg" alt="">
                                </figure>
                            </div>
                            <div class="col-md-8">
                                <div class="blog-details">
                                    <h4>Evangelical Free Church of America - Wikiwand</h4>
                                    <ul class="post-meta">
                                        <li><i class="fa fa-user"></i>Saugat Shrestha</li>
                                        <li><i class="fa fa-calendar"></i>14th March, 2019</li>
                                    </ul>
                                    <p>
                                        Contrary to popular belief, Lorem Ipsum is not simply random text. 
                                        It has roots in a piece of classical Latin literature from 45 BC, 
                                        making it over 2000 years old. Richard McClintock, a Latin professor 
                                        at Hampden-Sydney College in Virginia.
                                    </p>
                                    <a href="#" class="btn site-btn">READMORE</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="site-pagination">
                    <ul class="pagination">
                        <li><a href="#"><span><i class="fa fa-angle-left"></i></span></a></li>
                        <li class="active"><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#"><i class="fa fa-angle-right"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-3 col-sm-3">
                <div class="newsletter">
                    <h4 class="sidebartitle">NEWSLETTER</h4>
                    <p>Get Access to the latest tools, freebles, product announcements and much more !</p>
                    <form class="newsletterform">
                        <div class="form-group">
                            <input type="email" class="form-control" placeholder="Your Email">
                            <span class="input-icon"><i class="fa fa-envelope"></i></span> 
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn site-btn">Subscribe</button>
                        </div>
                    </form>
                </div>
                <div class="recentpostWrapper">
                    <h4 class="sidebartitle">RECENT POSTS</h4>
                    <div class="inner">
                        <div class="recentblock">
                            <a href="">
                                <figure class="thumbnail-img">
                                    <img src="images/blogimg1.jpg" alt="">
                                </figure>
                                <h5>EVANGELICAL FREE CHURCH OF AMERICA - WIKIWAND</h5>
                                <span><i class="fa fa-calendar"></i>14th March, 2019</span>
                            </a>
                        </div>
                        <div class="recentblock">
                            <a href="">
                                <figure class="thumbnail-img">
                                    <img src="images/blogimg2.jpg" alt="">
                                </figure>
                                <h5>EVANGELICAL FREE CHURCH OF AMERICA - WIKIWAND</h5>
                                <span><i class="fa fa-calendar"></i>14th March, 2019</span>
                            </a>
                        </div>
                        <div class="recentblock">
                            <a href="">
                                <figure class="thumbnail-img">
                                    <img src="images/blogimg3.jpg" alt="">
                                </figure>
                                <h5>EVANGELICAL FREE CHURCH OF AMERICA - WIKIWAND</h5>
                                <span><i class="fa fa-calendar"></i>14th March, 2019</span>
                            </a>
                        </div>
                        <div class="recentblock">
                            <a href="">
                                <figure class="thumbnail-img">
                                    <img src="images/blogimg4.jpg" alt="">
                                </figure>
                                <h5>EVANGELICAL FREE CHURCH OF AMERICA - WIKIWAND</h5>
                                <span><i class="fa fa-calendar"></i>14th March, 2019</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
		</div>
	</div>
</section><!-- /Page Content -->


<!-- Including Footer -->
<?php include('includes/footer.php'); ?>