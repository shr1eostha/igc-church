<!-- Including Header -->
<?php include('includes/header.php'); ?>


<!-- Page Banner -->
<div id="pagebanner" class="pagebanner events">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-6">
                <h3>CONTACT US</h3>
            </div>
            <div class="col-md-6 col-sm-6">
                <div class="breadcrumb">
                    <ul>
                        <li><a href="#">Home</a></li> 
                        <li class="active">Contact Us</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div><!-- /Page Banner -->



<section id="contactmap" class="contactmap">
	<div class="container">
		<div class="mapwrapper">
			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d56516.27689223729!2d85.29111335911601!3d27.709031933219393!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39eb198a307baabf%3A0xb5137c1bf18db1ea!2sKathmandu%2044600!5e0!3m2!1sen!2snp!4v1566892604922!5m2!1sen!2snp" width="100%" height="500" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
		</div>
	</div>
</section>


<!-- Page Content -->
<section id="content" class="pagecontent contact">
	<div class="container">
		<div class="contactformwrap">
			<div class="row">
				<div class="col-md-6 col-sm-6">
					<div class="contact-details">
						<div class="contact-block">
							<h2>Request a
								<span>Free Consultation</span>							
							</h2>
						</div>
						<div class="contact-block">
							<ul>
								<li><i class="fa fa-phone"></i> Phone : <span>+123 456 789</span></li>
								<li><i class="fa fa-envelope"></i> Email : <span>info@igckathmandu.com</span></li>
								<li><i class="fa fa-map-marker"></i> Location : <span>Kathmandu Nepal</span></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="col-md-6 col-sm-6">
					<div class="contactform">
						<form>
							<div class="row">
								<div class="col-md-6 col-sm-6">
									<div class="form-group">
										<label>Name: </label>
										<input type="text" class="form-control" name="name">
									</div>
								</div>
								<div class="col-md-6 col-sm-6">
									<div class="form-group">
										<label>Email: </label>
										<input type="email" class="form-control" name="email">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<label>Phone: </label>
										<input type="text" class="form-control" name="phone">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<label>Message: </label>
										<textarea name="message" class="form-control" rows="5"></textarea>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<button type="submit" class="btn site-btn">Send Now</button>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</section><!-- /Page Content -->




<!-- Including Footer -->
<?php include('includes/footer.php'); ?>