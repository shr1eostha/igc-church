<!-- Including Header -->
<?php include('includes/header.php'); ?>


<!-- Home Banner -->
<div id="homebanner" class="homebanner">
    <div class="bannercontentwrapper">
        <div class="container">
            <div class="bannercontent">
                <figure class="thumbnail-img">
                    <img src="images/site-logo1.png" alt="Banner Logo Image">
                </figure>
                <h1>Know God / Know Life</h1>
                <a href="#">
                    <img src="images/playicon.png" alt="Play Icon">
                    WATCH VIDEO
                </a>
            </div>
        </div>
    </div>
</div><!-- /Home Banner -->


<!-- Home Events -->
<div id="homeevents" class="homeevents">
    <div class="eventsblockwrapper">
        <div class="row">
            <div class="col-12 col-sm-6 col-md-3">
                <div class="eventsblock hasbg">
                    <div class="inner">
                        <span>UPCOMING</span>
                        <h2>EVENTS</h2>
                        <a href="#">
                            <figure class="thumbnail-img"></figure>
                            VIEW ALL
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-3">
                <div class="eventsblock">
                    <a href="">
                        <figure class="thumbnail-img">
                            <img src="images/event1.jpg" alt="Event 1">
                        </figure>
                        <div class="event-details">
                            <h5>Sunday Meet Up</h5>
                            <span class="postdate">15 november 2019</span>
                            <address>New Baneshwor, Kathmandu</address>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-3">
            <div class="eventsblock">
                    <a href="">
                        <figure class="thumbnail-img">
                            <img src="images/event2.jpg" alt="Event 2">
                        </figure>
                        <div class="event-details">
                            <h5>Sunday Meet Up</h5>
                            <span class="postdate">15 november 2019</span>
                            <address>New Baneshwor, Kathmandu</address>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-3">
                <div class="eventsblock">
                    <a href="">
                        <figure class="thumbnail-img">
                            <img src="images/event3.jpg" alt="Event 3">
                        </figure>
                        <div class="event-details">
                            <h5>Sunday Meet Up</h5>
                            <span class="postdate">15 november 2019</span>
                            <address>New Baneshwor, Kathmandu</address>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div><!-- /Home Events -->


<!-- Home Section Video -->
<section id="home-video" class="home_section video">
    <div class="container">
        <div class="section-title">
            <h3>Watch Church Sermons</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
        </div>
        <div class="row justify-content-center">
            <div class="col-12 col-sm-8">
                <div class="videoblock">
                <iframe width="100%" height="420" src="https://www.youtube.com/embed/fkjMMefZlMs" frameborder="0" 
                    allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen>
                </iframe>
                </div>
            </div>
            <div class="col-12 col-sm-4">
                <div class="videodetails">
                    <h4>Spiritual Grit - Part 1</h4>
                    <span>BY: MIKE BICKLE</span>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut 
                        labore et dolore magna aliqua.Lorem ipsum dolor sit amet.
                    </p>
                    <a href="" class="btn site-btn">
                        Watch All The Videos
                    </a>
                </div>
            </div>
        </div>
    </div>
</section><!-- /Home Section Video -->


<!-- Home About Section -->
<section id="home-about" class="home_section about">
    <div class="container">
        <div class="section-title">
            <h3>You Are Here! You are Blessed!</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit </p>
        </div>
        <div class="aboutblockwrapper">
            <div class="aboutcontent">
                <h4>About The Church</h4>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
                    Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. 
                </p>
                <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, 
                    consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore.
                </p>
                <a href="#" class="btn site-btn">Know More About Us</a>
            </div>
            <figure class="thumbnail-img">
                <img src="images/aboutblockimg.jpg" alt="About Block Image">
            </figure>
        </div>
    </div>
</section><!-- /Home About Section -->


<!-- Home Blog Section -->
<section id="home-blog" class="home_section blog">
    <div class="container">
        <div class="section-title">
            <h3>From The Blog</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit </p>
        </div>
        <div class="row">
            <div class="col-12 col-sm-8">
                <div class="blogsingleview">
                    <figure class="thumbnail-img">
                        <img src="images/blogimg1.jpg" alt="Blog Single Image">
                    </figure>
                    <div class="blogdetails">
                        <h5>Lorem ipsum dolor sit amet, consectetur</h5>
                        <ul class="post-meta">
                            <li>By: Admin</li>
                            <li>Date: 05 Aug 2019</li>
                        </ul>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                            Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat...
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-4">
                <div class="bloglistviewWrapper">
                    <div class="bloglistview">
                        <a href="#">
                            <figure class="thumbnail-img">
                                <img src="images/blogimg2.jpg" alt="Blog Image">
                            </figure>
                            <div class="blogdetails">
                                <h5>Lorem ipsum dolor sit amet, consectetur</h5>
                                <ul class="post-meta">
                                    <li>By: Admin</li>
                                    <li>Date: 05 Aug 2019</li>
                                </ul>
                            </div>
                        </a>
                    </div>
                    <div class="bloglistview">
                        <a href="#">
                            <figure class="thumbnail-img">
                                <img src="images/blogimg3.jpg" alt="Blog Image">
                            </figure>
                            <div class="blogdetails">
                                <h5>Lorem ipsum dolor sit amet, consectetur</h5>
                                <ul class="post-meta">
                                    <li>By: Admin</li>
                                    <li>Date: 05 Aug 2019</li>
                                </ul>
                            </div>
                        </a>
                    </div>
                    <div class="bloglistview">
                        <a href="#">
                            <figure class="thumbnail-img">
                                <img src="images/blogimg4.jpg" alt="Blog Image">
                            </figure>
                            <div class="blogdetails">
                                <h5>Lorem ipsum dolor sit amet, consectetur</h5>
                                <ul class="post-meta">
                                    <li>By: Admin</li>
                                    <li>Date: 05 Aug 2019</li>
                                </ul>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="btnwrapper">
                    <a href="" class="btn site-btn">VIEW ALL THE BLOG</a>
                </div>
            </div>
        </div>
    </div>
</section><!-- /Home Blog Section -->


<!-- Home Testimonials Section -->
<section id="home-testimonials" class="home_section testimonials">
    <div class="container">
        <div class="section-title">
            <h3>Testimonials</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
        </div>
        <div class="slider-container">
            <div class="testimonials-slider">
                <div class="slider-item">
                    <p class="desc">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                        Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo.
                    </p>
                    <div class="slider-details">
                        <p>
                            Mike Bickle
                            <span>Finance & HR Manager</span>
                        </p>
                    </div>
                </div>
                <div class="slider-item">
                    <p class="desc">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                        Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo.
                    </p>
                    <div class="slider-details">
                        <p>
                            Mike Bickle
                            <span>Finance & HR Manager</span>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section><!-- /Home Testimonials Section -->


<!-- Home Subscribe Section -->
<section id="home-subscribe" class="home_section subscribe">
    <div class="container">
        <div class="section-title">
            <h3>Subscribe To Our Newsletter</h3>
            <p>Be Updated with us</p>
        </div>
        <div class="subscribeform">
            <form class="form">
                <div class="form-group">
                    <input  type="text" class="form-control" placeholder="Name">
                </div>
                <div class="form-group">
                    <input  type="text" class="form-control" placeholder="Email">
                </div>
                <div class="form-group">
                    <input  type="text" class="form-control" placeholder="Phone">
                </div>
                <div class="form-group">
                   <button type="submit" class="btn site-btn">SUBSCRIBE</button>
                </div>
            </form>
        </div>
    </div>
</section><!-- /Home Subscribe Section -->



<!-- Including Footer -->
<?php include('includes/footer.php'); ?>