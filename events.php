<!-- Including Header -->
<?php include('includes/header.php'); ?>


<!-- Page Banner -->
<div id="pagebanner" class="pagebanner events">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-6">
                <h3>OUR EVENTS</h3>
            </div>
            <div class="col-md-6 col-sm-6">
                <div class="breadcrumb">
                    <ul>
                        <li><a href="#">Home</a></li> 
                        <li class="active">Our Events</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div><!-- /Page Banner -->


<!-- Page Content -->
<section id="content" class="pagecontent events">
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-sm-6">
				<div class="eventsblock isstatic">
                    <a href="">
                        <div class="row">
                            <div class="co-md-8 col-sm-8">
                                <figure class="thumbnail-img">
                                    <img src="images/events1.jpg" alt="Events 1">
                                </figure>
                            </div>
                            <div class="co-md-4 col-sm-4">
                                <div class="event-date">
                                    29th December <span>2019</span>
                                </div>
                            </div>
                        </div>
                        <div class="event-details">
                            <h5>Sunday Meet Up</h5>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt 
                                ut labore et dolore magna aliqua.Lorem ipsum dolor sit amet.
                            </p>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-md-6 col-sm-6">
				<div class="eventsblock isstatic">
                    <a href="">
                        <div class="row">
                            <div class="co-md-8 col-sm-8">
                                <figure class="thumbnail-img">
                                    <img src="images/events1.jpg" alt="Events 1">
                                </figure>
                            </div>
                            <div class="co-md-4 col-sm-4">
                                <div class="event-date">
                                    29th December <span>2019</span>
                                </div>
                            </div>
                        </div>
                        <div class="event-details">
                            <h5>Sunday Meet Up</h5>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt 
                                ut labore et dolore magna aliqua.Lorem ipsum dolor sit amet.
                            </p>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-md-6 col-sm-6">
				<div class="eventsblock isstatic">
                    <a href="">
                        <div class="row">
                            <div class="co-md-8 col-sm-8">
                                <figure class="thumbnail-img">
                                    <img src="images/events1.jpg" alt="Events 1">
                                </figure>
                            </div>
                            <div class="co-md-4 col-sm-4">
                                <div class="event-date">
                                    29th December <span>2019</span>
                                </div>
                            </div>
                        </div>
                        <div class="event-details">
                            <h5>Sunday Meet Up</h5>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt 
                                ut labore et dolore magna aliqua.Lorem ipsum dolor sit amet.
                            </p>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-md-6 col-sm-6">
				<div class="eventsblock isstatic">
                    <a href="">
                        <div class="row">
                            <div class="co-md-8 col-sm-8">
                                <figure class="thumbnail-img">
                                    <img src="images/events1.jpg" alt="Events 1">
                                </figure>
                            </div>
                            <div class="co-md-4 col-sm-4">
                                <div class="event-date">
                                    29th December <span>2019</span>
                                </div>
                            </div>
                        </div>
                        <div class="event-details">
                            <h5>Sunday Meet Up</h5>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt 
                                ut labore et dolore magna aliqua.Lorem ipsum dolor sit amet.
                            </p>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-md-6 col-sm-6">
				<div class="eventsblock isstatic">
                    <a href="">
                        <div class="row">
                            <div class="co-md-8 col-sm-8">
                                <figure class="thumbnail-img">
                                    <img src="images/events1.jpg" alt="Events 1">
                                </figure>
                            </div>
                            <div class="co-md-4 col-sm-4">
                                <div class="event-date">
                                    29th December <span>2019</span>
                                </div>
                            </div>
                        </div>
                        <div class="event-details">
                            <h5>Sunday Meet Up</h5>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt 
                                ut labore et dolore magna aliqua.Lorem ipsum dolor sit amet.
                            </p>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-md-6 col-sm-6">
				<div class="eventsblock isstatic">
                    <a href="">
                        <div class="row">
                            <div class="co-md-8 col-sm-8">
                                <figure class="thumbnail-img">
                                    <img src="images/events1.jpg" alt="Events 1">
                                </figure>
                            </div>
                            <div class="co-md-4 col-sm-4">
                                <div class="event-date">
                                    29th December <span>2019</span>
                                </div>
                            </div>
                        </div>
                        <div class="event-details">
                            <h5>Sunday Meet Up</h5>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt 
                                ut labore et dolore magna aliqua.Lorem ipsum dolor sit amet.
                            </p>
                        </div>
                    </a>
                </div>
			</div>
		</div>
	</div>
</section><!-- /Page Content -->


<!-- Including Footer -->
<?php include('includes/footer.php'); ?>