<!-- Including Header -->
<?php include('includes/header.php'); ?>


<!-- Page Banner -->
<div id="pagebanner" class="pagebanner events">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-6">
                <h3>VIDEOS</h3>
            </div>
            <div class="col-md-6 col-sm-6">
                <div class="breadcrumb">
                    <ul>
                        <li><a href="#">Home</a></li> 
                        <li class="active">Videos</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div><!-- /Page Banner -->


<!-- Page Content -->
<section id="content" class="pagecontent blog">
	<div class="container">
		<div class="row">
			<div class="col-md-9 col-sm-9">
				<div class="blogListWrapper">
                    <div class="blogblock video">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="videoimgwrapper">
                                    <figure class="thumbnail-img">
                                        <img src="images/blogimg2.jpg" alt="">
                                        <span class="play-icon">
                                            <i class="fa fa-play"></i>
                                        </span>
                                    </figure>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="blog-details">
                                    <h4>Evangelical Orthodox Catholic Church</h4>
                                    <ul class="post-meta">
                                        <li><i class="fa fa-calendar"></i>Posted on: 25th December, 2019</li>
                                    </ul>
                                    <p>
                                        This video was posted on 25th of december. Christmas Day
                                    </p>
                                    <a href="#" class="btn site-btn video-btn" data-toggle="modal" data-src="https://www.youtube.com/embed/9zGLAl-Q1-o" data-target="#myModal">WATCH VIDEO</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="blogblock video">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="videoimgwrapper">
                                    <figure class="thumbnail-img">
                                        <img src="images/blogimg2.jpg" alt="">
                                        <span class="play-icon">
                                            <i class="fa fa-play"></i>
                                        </span>
                                    </figure>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="blog-details">
                                    <h4>Evangelical Orthodox Catholic Church</h4>
                                    <ul class="post-meta">
                                        <li><i class="fa fa-calendar"></i>Posted on: 25th December, 2019</li>
                                    </ul>
                                    <p>
                                        This video was posted on 25th of december. Christmas Day
                                    </p>
                                    <a href="#" class="btn site-btn video-btn" data-toggle="modal" data-src="https://www.youtube.com/embed/9zGLAl-Q1-o" data-target="#myModal">WATCH VIDEO</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="blogblock video">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="videoimgwrapper">
                                    <figure class="thumbnail-img">
                                        <img src="images/blogimg2.jpg" alt="">
                                        <span class="play-icon">
                                            <i class="fa fa-play"></i>
                                        </span>
                                    </figure>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="blog-details">
                                    <h4>Evangelical Orthodox Catholic Church</h4>
                                    <ul class="post-meta">
                                        <li><i class="fa fa-calendar"></i>Posted on: 25th December, 2019</li>
                                    </ul>
                                    <p>
                                        This video was posted on 25th of december. Christmas Day
                                    </p>
                                    <a href="#" class="btn site-btn video-btn" data-toggle="modal" data-src="https://www.youtube.com/embed/9zGLAl-Q1-o" data-target="#myModal">WATCH VIDEO</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="blogblock video">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="videoimgwrapper">
                                    <figure class="thumbnail-img">
                                        <img src="images/blogimg2.jpg" alt="">
                                        <span class="play-icon">
                                            <i class="fa fa-play"></i>
                                        </span>
                                    </figure>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="blog-details">
                                    <h4>Evangelical Orthodox Catholic Church</h4>
                                    <ul class="post-meta">
                                        <li><i class="fa fa-calendar"></i>Posted on: 25th December, 2019</li>
                                    </ul>
                                    <p>
                                        This video was posted on 25th of december. Christmas Day
                                    </p>
                                    <a href="#" class="btn site-btn video-btn" data-toggle="modal" data-src="https://www.youtube.com/embed/9zGLAl-Q1-o" data-target="#myModal">WATCH VIDEO</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="blogblock video">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="videoimgwrapper">
                                    <figure class="thumbnail-img">
                                        <img src="images/blogimg2.jpg" alt="">
                                        <span class="play-icon">
                                            <i class="fa fa-play"></i>
                                        </span>
                                    </figure>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="blog-details">
                                    <h4>Evangelical Orthodox Catholic Church</h4>
                                    <ul class="post-meta">
                                        <li><i class="fa fa-calendar"></i>Posted on: 25th December, 2019</li>
                                    </ul>
                                    <p>
                                        This video was posted on 25th of december. Christmas Day
                                    </p>
                                    <a href="#" class="btn site-btn video-btn" data-toggle="modal" data-src="https://www.youtube.com/embed/9zGLAl-Q1-o" data-target="#myModal">WATCH VIDEO</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="site-pagination">
                    <ul class="pagination">
                        <li><a href="#"><span><i class="fa fa-angle-left"></i></span></a></li>
                        <li class="active"><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#"><i class="fa fa-angle-right"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-3 col-sm-3">
                <div class="newsletter">
                    <h4 class="sidebartitle">NEWSLETTER</h4>
                    <p>Get Access to the latest tools, freebles, product announcements and much more !</p>
                    <form class="newsletterform">
                        <div class="form-group">
                            <input type="email" class="form-control" placeholder="Your Email">
                            <span class="input-icon"><i class="fa fa-envelope"></i></span> 
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn site-btn">Subscribe</button>
                        </div>
                    </form>
                </div>
                <div class="recentpostWrapper">
                    <h4 class="sidebartitle">RECENT POSTS</h4>
                    <div class="inner">
                        <div class="recentblock">
                            <a href="">
                                <figure class="thumbnail-img">
                                    <img src="images/blogimg1.jpg" alt="">
                                </figure>
                                <h5>EVANGELICAL FREE CHURCH OF AMERICA - WIKIWAND</h5>
                                <span><i class="fa fa-calendar"></i>14th March, 2019</span>
                            </a>
                        </div>
                        <div class="recentblock">
                            <a href="">
                                <figure class="thumbnail-img">
                                    <img src="images/blogimg2.jpg" alt="">
                                </figure>
                                <h5>EVANGELICAL FREE CHURCH OF AMERICA - WIKIWAND</h5>
                                <span><i class="fa fa-calendar"></i>14th March, 2019</span>
                            </a>
                        </div>
                        <div class="recentblock">
                            <a href="">
                                <figure class="thumbnail-img">
                                    <img src="images/blogimg3.jpg" alt="">
                                </figure>
                                <h5>EVANGELICAL FREE CHURCH OF AMERICA - WIKIWAND</h5>
                                <span><i class="fa fa-calendar"></i>14th March, 2019</span>
                            </a>
                        </div>
                        <div class="recentblock">
                            <a href="">
                                <figure class="thumbnail-img">
                                    <img src="images/blogimg4.jpg" alt="">
                                </figure>
                                <h5>EVANGELICAL FREE CHURCH OF AMERICA - WIKIWAND</h5>
                                <span><i class="fa fa-calendar"></i>14th March, 2019</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
		</div>
	</div>
</section><!-- /Page Content -->


<!-- Video Modal PopupBox -->
<div class="modal fade videomodal" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>        
                <div class="embed-responsive embed-responsive-16by9">
                    <iframe class="embed-responsive-item" src="" id="video"  allowscriptaccess="always" allow="autoplay"></iframe>
                </div>
            </div>
        </div>
    </div>
</div> 


<!-- Including Footer -->
<?php include('includes/footer.php'); ?>