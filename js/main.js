$(document).ready(function(){

    // Sticky Header
    $(window).scroll(function() {
        if ($(this).scrollTop() > 50){  
            $('.site-header').addClass("sticky");
        }
        else{
            $('.site-header').removeClass("sticky");
        }
    });

    // Site Navbar 
    $('#navbar-toggle').click(function(){
        $('.site-navigation').slideToggle();
    });


    // Testimonials Slider
    $('.testimonials-slider').bxSlider({
        controls: false,
        pager: false,
        auto: true
    });


    // Video
    var $videoSrc;  
    $('.video-btn').click(function() {
        $videoSrc = $(this).data( "src" );
    });
    console.log($videoSrc);
    
    $('#myModal').on('shown.bs.modal', function (e) {
        $("#video").attr('src',$videoSrc + "?autoplay=1&amp;modestbranding=1&amp;showinfo=0" ); 
    })          
    $('#myModal').on('hide.bs.modal', function (e) {            
        $("#video").attr('src',$videoSrc); 
    }) 
    
});