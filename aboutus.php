<!-- Including Header -->
<?php include('includes/header.php'); ?>


<!-- Page Banner -->
<div id="pagebanner" class="pagebanner">
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-sm-6">
				<h3>ABOUT US</h3>
			</div>
			<div class="col-md-6 col-sm-6">
				<div class="breadcrumb">
					<ul>
						<li><a href="#">Home</a></li> 
						<li class="active">About Us</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div><!-- /Page Banner -->


<!-- Section About Main Content  -->
<section id="aboutchurch" class="pagecontent aboutchurch">
	<div class="container">
		<div class="row">
			<div class="col-md-5">
				<div class="churchcontent">
					<h3>We are a Church That Believes in <span>Jesus</span></h3>
					<p>
						Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 
						'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years,
					</p>
					<p>
						You need to be sure there isn't anything embarrassing hidden in the middle of text.
					</p>
					<p>
						Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 
						'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years,
					</p>
					<a href="#" class="btn site-btn">LEARN MORE</a>
				</div>
			</div>
			<div class="col-md-7">
				<div class="churchimg">
					<figure class="thumbnail-img text-right">
						<img src="images/churchbg.png" alt="church">
					</figure>
					<div class="videothumb">
						<img src="images/videothumb.jpg" alt="video thumb">
						<a href="#" class="videoplay">
							<i class="fa fa-play"></i>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- /Section About Main Content -->


<!-- Section Our Members -->
<!-- <section id="members" class="pagecontent members">
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-sm-6"></div>
			<div class="col-md-6 col-sm-6"></div>
		</div>
	</div>
</section> -->
<!-- /Section Our Member -->


<!-- Section Become Volunteer -->
<section id="becomevc" class="pagecontent becomevc">
	<div class="container">
		<div class="volunteerWrapper">
			<div class="row">
				<div class="col-md-6">
					<div class="inner">
						<h4>Become a Volunteer</h4>
						<p>
							Packages and web page editors now use Lorem Ipsum as their default model text, and a search for 
							'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years
						</p>
						<a href="" class="btn site-btn">Apply Now</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- /Section Become Volunteer -->



<!-- Section Pastors -->
<section id="pagecontent" class="pagecontent pastors">
	<div class="container">
		<div class="section-title">
            <h3>OUR PASTORS</h3>
            <p>Will uncover many web sites still in their infancy. Various versions have Evolved over the years</p>
        </div>
		<div class="pastorsblockWrapper">
			<div class="row">
				<div class="col-md-3 col-sm-6">
					<div class="pastors-block">
						<figure class="thumbnail-img">
							<img src="images/pastorimg1.jpg" alt="Pastor Image 1">
						</figure>
						<div class="pastors-info">
							<h4>David Dahan</h4>
							<p>Lead Pastor</p>
						</div>
					</div>
				</div>
				<div class="col-md-3 col-sm-6">
					<div class="pastors-block">
						<figure class="thumbnail-img">
							<img src="images/pastorimg2.jpg" alt="Pastor Image 2">
						</figure>
						<div class="pastors-info">
							<h4>Kenne G. Patten</h4>
							<p>Lead Pastor</p>
						</div>
					</div>
				</div>
				<div class="col-md-3 col-sm-6">
					<div class="pastors-block">
						<figure class="thumbnail-img">
							<img src="images/pastorimg3.jpg" alt="Pastor Image 3">
						</figure>
						<div class="pastors-info">
							<h4>Kim I. Bailey</h4>
							<p>Lead Pastor</p>
						</div>
					</div>
				</div>
				<div class="col-md-3 col-sm-6">
					<div class="pastors-block">
						<figure class="thumbnail-img">
							<img src="images/pastorimg4.jpg" alt="Pastor Image 4">
						</figure>
						<div class="pastors-info">
							<h4>Kathy Santos</h4>
							<p>Lead Pastor</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section><!-- /Section Pastors -->






<!-- Including Footer -->
<?php include('includes/footer.php'); ?>