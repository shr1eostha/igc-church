<!-- Including Header -->
<?php include('includes/header.php'); ?>


<!-- Page Banner -->
<div id="pagebanner" class="pagebanner single">
	<div class="container">
		<div class="row">
			<div class="col-md-8">
				<h3>Bhutan government to waive off the royalty for the visit <br> to eastern Bhutan!</h3>
			</div>
			<div class="col-md-4">
				<div class="breadcrumb">
                    <ul>
                        <li><a href="#">Home</a></li> 
                        <li class="active">Blog</li>
                        <li class="active">Single Blog</li>
                    </ul>
                </div>
			</div>
		</div>
	</div>
</div><!-- /Page Banner -->


<!-- Page Content Single -->
<section id="pagecontent" class="pagecontent single">
	<div class="container">
		<div class="row">
			<div class="col-md-9 col-sm-9">
				<div class="single-post">
					<figure class="thumbnail-img">
						<img src="images/single-img.jpg" alt="Single Image">
					</figure>
					<div class="blog-details">
                        <h4>Evangelical Orthodox Catholic Church</h4>
                        <ul class="post-meta">
                            <li><i class="fa fa-user"></i>Saugat Shrestha</li>
                            <li><i class="fa fa-calendar"></i>14th March, 2019</li>
                        </ul>
                        <blockquote>
	                        <p>
	                            Contrary to popular belief, Lorem Ipsum is not simply random text. 
	                            It has roots in a piece of classical Latin literature from 45 BC, 
	                            making it over 2000 years old. Richard McClintock, a Latin professor 
	                            at Hampden-Sydney College in Virginia. Contrary to popular belief, Lorem Ipsum is not simply random text. 
	                            It has roots in a piece of classical Latin literature from 45 BC, 
	                            making it over 2000 years old. Richard McClintock, a Latin professor 
	                            at Hampden-Sydney College in Virginia.
	                        </p>
	                        <p>
	                            Contrary to popular belief, Lorem Ipsum is not simply random text. 
	                            It has roots in a piece of classical Latin literature from 45 BC, 
	                            making it over 2000 years old. Richard McClintock, a Latin professor 
	                            at Hampden-Sydney College in Virginia. Contrary to popular belief, Lorem Ipsum is not simply random text. 
	                            It has roots in a piece of classical Latin literature from 45 BC, 
	                            making it over 2000 years old. Richard McClintock, a Latin professor 
	                            at Hampden-Sydney College in Virginia.
	                        </p>
                        </blockquote>
                    </div>
				</div>
				<div class="postshare">
					<div class="row">
						<div class="col-md-6 col-sm-6">
							<h6>SHARE NOW</h6>
							<ul>
								<li><a href="#" class="fb"><i class="fa fa-facebook"></i></a></li>
								<li><a href="#" class="tw"><i class="fa fa-twitter"></i></a></li>
								<li><a href="#" class="lin"><i class="fa fa-linkedin"></i></a></li>
								<li><a href="#" class="ins"><i class="fa fa-instagram"></i></a></li>
							</ul>
						</div>
						<div class="col-md-6 col-sm-6">
							<p class="totalcomments">
								<i class="fa fa-comment"></i> 25 Comments
							</p>
						</div>
					</div>
					
				</div>
			</div>
			<div class="col-md-3 col-sm-3">
                <div class="newsletter">
                    <h4 class="sidebartitle">NEWSLETTER</h4>
                    <p>Get Access to the latest tools, freebles, product announcements and much more !</p>
                    <form class="newsletterform">
                        <div class="form-group">
                            <input type="email" class="form-control" placeholder="Your Email">
                            <span class="input-icon"><i class="fa fa-envelope"></i></span> 
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn site-btn">Subscribe</button>
                        </div>
                    </form>
                </div>
                <div class="recentpostWrapper">
                    <h4 class="sidebartitle">RECENT POSTS</h4>
                    <div class="inner">
                        <div class="recentblock">
                            <a href="">
                                <figure class="thumbnail-img">
                                    <img src="images/blogimg1.jpg" alt="">
                                </figure>
                                <h5>EVANGELICAL FREE CHURCH OF AMERICA - WIKIWAND</h5>
                                <span><i class="fa fa-calendar"></i>14th March, 2019</span>
                            </a>
                        </div>
                        <div class="recentblock">
                            <a href="">
                                <figure class="thumbnail-img">
                                    <img src="images/blogimg2.jpg" alt="">
                                </figure>
                                <h5>EVANGELICAL FREE CHURCH OF AMERICA - WIKIWAND</h5>
                                <span><i class="fa fa-calendar"></i>14th March, 2019</span>
                            </a>
                        </div>
                        <div class="recentblock">
                            <a href="">
                                <figure class="thumbnail-img">
                                    <img src="images/blogimg3.jpg" alt="">
                                </figure>
                                <h5>EVANGELICAL FREE CHURCH OF AMERICA - WIKIWAND</h5>
                                <span><i class="fa fa-calendar"></i>14th March, 2019</span>
                            </a>
                        </div>
                        <div class="recentblock">
                            <a href="">
                                <figure class="thumbnail-img">
                                    <img src="images/blogimg4.jpg" alt="">
                                </figure>
                                <h5>EVANGELICAL FREE CHURCH OF AMERICA - WIKIWAND</h5>
                                <span><i class="fa fa-calendar"></i>14th March, 2019</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
		</div>
	</div>
</section>




<!-- Including Footer -->
<?php include('includes/footer.php'); ?>